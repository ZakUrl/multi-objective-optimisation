
import json


class Data:
	
	""" Get data and manipulate from Json  """
	
	TAX = 0.7
	TOWNNUMBER = 3
	NATIONALBANK = "0"
	
	def __init__(self, file):
		self.file = file
		with open(file) as json_file:  
		    self.data = json.load(json_file)

	def getTowns(self):
		return [town for town in self.data]

	def getTotalAmount(self):
		totalAmount = 0
		for town in self.data:
			totalAmount += self.data[town]['population']
		totalAmount *= self.TAX
		return totalAmount

	def getName(self,town):
		return self.data[town]["name"]

	def getMostPopulatedTown(self):
		#get towns
		mostPopulated = [town for town in self.data]
		#sort them  by population (descent order)
		mostPopulated = sorted(mostPopulated,key=lambda town:self.data[town]['population'],reverse=True)
		#take the first three
		mostPopulated = mostPopulated[:self.TOWNNUMBER]
		return mostPopulated

	def getSumForTown(self,town):
		return int(self.data[town]["sum"])

	def getDistance(self,town1,town2):
	    """ returns distance between two points"""
	    return self.data[town1]["distances"][int(town2)]



		