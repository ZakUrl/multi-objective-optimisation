from random import randint,choice
from copy import deepcopy

#The probability of mutation
PROB = 10


def crossover(sol1,sol2):
    """ crosses over to solution given in parameters"""
    key1 = choice(list(sol1.keys()))
    key2 = choice(list(sol2.keys()))
    chromo1 = sol1[key1]
    chromo2 = sol2[key2]
    chromo1FirstPt,chromo1SecondPt = twoPoints(chromo1)
    chromo2FirstPt,chromo2SecondPt = twoPoints(chromo2)
    child1 = deepcopy(sol1)
    child2 = deepcopy(sol2) 
    child1[key1] = chromo2[:chromo2FirstPt] + chromo1[chromo1FirstPt:chromo1SecondPt] + chromo2[chromo2SecondPt+1:]
    child2[key2] = chromo1[:chromo1FirstPt] + chromo2[chromo2FirstPt:chromo2SecondPt] + chromo1[chromo1SecondPt+1:]
    return child1,child2

def twoPoints(lst):
    """ returns two random points from a list """
    if len(lst) > 2:
        firstPt = randint(0,len(lst)//2)
        secondPt = randint((len(lst)//2)+1,len(lst)-1)
    else:
        firstPt = randint(0,len(lst)//2)
        secondPt = randint(len(lst)//2,len(lst)//2)
    return firstPt,secondPt
    
def mutation(sol):
    """ modifies the solution by a prob of 10% """
    if randint(0,100) < PROB:
        gen = choice(list(sol.keys()))
        chromo = sol[gen]
        firstPt,secondPt = twoPoints(chromo)
        mutationZone = chromo[firstPt:secondPt+1]
        mutationZone.reverse()
        sol[gen] = chromo[:firstPt] + mutationZone + chromo[secondPt+1:]