##########################
#   Hamouchi Zakariya    #
#   Cherrabi Mustapha    #
#   El Moussaoui Bilal   #
##########################
from nsga2 import NsgaII
from data import Data
from time import time
from csvcreator import CSVCreator

def main():
    tmax = 5000
    d = Data('populationBrussels.json')
    n = NsgaII(d,tmax)
    t1 = time()
    n.launch()
    t2 = time()
    print("It took ",t2-t1," seconds")
    solutions = n.getSol()
    c = CSVCreator("res.csv",solutions)
    c.create()
    n.display()



if __name__ == '__main__':
    main()