from crossover import crossover, mutation
from random import randint, choice
import matplotlib.pyplot as plt
import collections
import numpy as np
import pandas as pd


def combinatorics(res):
    """ returns the different combs that equals a given 'res' """
    comb = []
    for i in range(res+1):
        for j in range(res+1):
            for k in range(res+1):
                if (i + j + k) == res:
                    comb.append([i,j,k])
    return comb

def pickTwoFrom(ls):
    """ Returns two random different elem from a list"""
    ind = randint(0,len(ls)-1)
    ind2 = randint(0,len(ls)-1)
    while ind == ind2:
        ind = randint(0,len(ls)-1)
    return ls[ind],ls[ind2]


class NsgaII:
    """ Solve the problem by using the NSGAII algorithm """

    NUMBEROFTOWN = 19

    def __init__(self,data,tmax):
        self.data = data
        self.tmax = tmax
        self.sol = None

    def launch(self):
        """ Create a first population and children and launch the algorithm """
        parents = self.createPopulation()
        children = self.createChildren(parents)
        self.pareto = self.nsga2(parents,children)

    def display(self):
        """ Displays the solutions (the first pareto front is linked in red)"""
        paretoFronts = []
        for i in range(len(self.pareto)):
            front = []
            for pair in self.pareto[i]:
                pt = [sum(pair[0]),sum(pair[1])]
                front.append(pt)
            paretoFronts.append(front)
        firstParetoFront = paretoFronts[0]
        firstParetoFront = pd.DataFrame(firstParetoFront)
        firstParetoFront.sort_values(0, inplace=True)
        firstParetoFront = firstParetoFront.values
        rsksPareto = firstParetoFront[:, 0]
        dstPareto = firstParetoFront[:, 1]
        rsks2 = [sum(pair[0]) for i in range(len(self.pareto)) for pair in self.pareto[i]]
        dst2 = [sum(pair[1]) for i in range(len(self.pareto)) for pair in self.pareto[i]]
        plt.scatter(rsks2,dst2)
        plt.plot(rsksPareto,dstPareto,color='r')
        plt.ylabel("Distance")
        plt.xlabel("Risk")
        plt.show()

    def dispSol(self,rt):
        """ Useful for debugging and check the content of solution """
        for i in range(len(rt)):
            print("-------------------------------")
            for c in rt[i]:
                print(" tournée ",c," : ")
                for sp in rt[i][c]:
                    print(self.data.getName(sp),end=" , ")
                print("     argent :",sum([self.getSumForTown(t) for t in rt[i][c]]))
            print()

    def getSol(self):
        """ Returns the solutions of the first pareto front"""
        if self.sol:
            solutionsAmount = []
            for i in range(len(self.sol)):
                sol = {}
                for c in self.sol[i]:
                    temp = [0]
                    amount = 0
                    for town in self.sol[i][c]:
                        if town == self.data.NATIONALBANK:
                            amount = 0
                        amount += self.getSumForTown(town)
                        temp.append(amount)
                    sol[c] = temp
                solutionsAmount.append(sol)
            return self.sol, solutionsAmount



    def fill(self,tour,n,towns):
        """ fill a tour with 'n' town from towns """
        for i in range(n):
            town = choice(towns)
            tour.append(town)
            towns.remove(town)
        #why not adding some return to the national bank by a prob of 50%
        #if randint(0,1):
        #we consider that a tour should be bigger than 3 to go back to the NB
        if n > 3:
            numberOfReturn = randint(1, n//2)
            possible = list(range(1,n-2))
            lastElem = -1
            for i in range(numberOfReturn):
                if len(possible):
                    idxOfInsertion = choice(possible)
                    #don't insert 0 next to each other
                    if tour[idxOfInsertion-1] != self.data.NATIONALBANK and tour[idxOfInsertion] != self.data.NATIONALBANK:
                        idxOfInsertion = choice(possible)
                        tour.insert(idxOfInsertion,self.data.NATIONALBANK)
                        possible.remove(idxOfInsertion)
                        lastElem = idxOfInsertion


    def getDistance(self,town1,town2):
        return self.data.getDistance(town1,town2)

    def getTotalAmount(self):
        return self.data.getTotalAmount()

    def getMostPopulatedTown(self):
        return self.data.getMostPopulatedTown()

    def getSumForTown(self,town):
        return self.data.getSumForTown(town)

    def avoidThreeBiggest(self,solution):
        """ First constraints => Check if a truck visits three most popluated towns """
        mostPopulated = self.getMostPopulatedTown()
        for c in solution:
            if len(solution[c]) >= 3:
                f,s,t = mostPopulated
                if f in solution[c] and s in solution[c] and t in solution[c]:
                    return False
        return True

    def checkAmount(self,solution):
        """ Second constraints =>  check that a truck doesn't carry half of the amount during a tour"""
        totalAmount = self.getTotalAmount()
        for c in solution:
            amount = 0
            for town in solution[c]:
                amount += self.getSumForTown(town)
            if amount > totalAmount/2:
                return False
        return True

    def minimizeDistance(self,missingTowns,sol):
        """ When inserting missing tonws in tour insert the towns where the distance is minimized """
        for t in missingTowns:
            totD = 1000000
            for c in sol:
                for i in range(len(sol[c])-1):
                    d1 = self.getDistance(str(t),sol[c][i])
                    d2 = self.getDistance(str(t),sol[c][i+1])
                    if d1 + d2 < totD:
                        totD = d1 + d2
                        ind = i
                        sp = c
            sol[sp].insert(ind,str(t))


    def addMissingTowns(self,solution):
        """ When the crossover happens there are some towns that disappears we need to add them to the solution """
        towns = set(range(1,20))
        solTowns = set()
        for c in solution:
            for t in solution[c]:
                solTowns.add(int(t))
        missingTowns = towns - solTowns
        #check where we want to insert the missing towns => insert where it minimizes the distance
        self.minimizeDistance(missingTowns,solution)

    def removeDuplicates(self,solution):
        """ When the crossover happens some towns are duplicated we have to remove them in the tour of a truck
            and between the different trucks
        """
        temp = []
        #remove duplicate from a truck tour
        #can't do it with a set because the order wouldn't stay the same
        for c in solution:
            for town in solution[c]:
                if town == self.data.NATIONALBANK or not(town in temp):
                    temp.append(town)
            solution[c] = temp
            temp = []
        #remove duplicate between truck tour
        for i in range(1,20):
            count = 0
            tour = []
            for c in solution:
                if str(i) in solution[c]:
                    count += 1
                    tour.append(c)
            if count > 1:
                solution[choice(tour)].remove(str(i))

    def correctReturns(self,solution):
        """ Since a truck during its tour can go back to the national bank
            sometimes there are duplicated "0"(national bank) so we have to remove them
        """
        for c in solution:
            idxs = []
            #if first elem is bank
            if solution[c] and solution[c][0] == self.data.NATIONALBANK:
                idxs.append(0)
            for i in range(1,len(solution[c])-1):
                #f two banks following
                if solution[c][i] == self.data.NATIONALBANK and solution[c][i+1] == self.data.NATIONALBANK:
                    idxs.append(i)
            if solution[c] and solution[c][len(solution[c])-1] == self.data.NATIONALBANK:
                idxs.append(len(solution[c])-1)
            gap = 0
            for ind in idxs:
                if len(solution[c]):
                    del solution[c][ind-gap]
                    gap += 1


    def correctSolution(self,solution):
        """ When the crossover happens we have to correct the solutions """ 
        self.correctReturns(solution)
        self.removeDuplicates(solution)
        self.addMissingTowns(solution)


    #CREATES INITIAL POPULATION
    def createPopulation(self):
        """ returns a population of valid but not necesseraly optimal solutions """
        population = []
        #generate the combinations
        comb = combinatorics(self.NUMBEROFTOWN)
        for c in comb:
            #we do not take the first spot because it's the national bank
            towns = self.data.getTowns()[1:]
            solution = {'C1':[],'C2':[],'C3':[]}
            #truck => 'C1','C2',etc
            #size => number of towns that the truck go through
            for truck,size in zip(solution,c):
                self.fill(solution[truck],size,towns)
            self.correctReturns(solution)
            if self.avoidThreeBiggest(solution) and self.checkAmount(solution):
                population.append(solution)
        return population

    def createChildren(self,parents):
        """ Create a population of children from a population of parents"""
        children = []
        while len(children) < len(parents):
            parent1,parent2 = pickTwoFrom(parents)
            child1,child2 = crossover(parent1,parent2)
            #occurs with a certain probability
            mutation(child1)
            mutation(child2)
            self.correctSolution(child1)
            self.correctSolution(child2)
            if self.avoidThreeBiggest(child1) and self.checkAmount(child1):
                children.append(child1)
            if self.avoidThreeBiggest(child2) and self.checkAmount(child2):
                children.append(child2)
        return children

    def computeDistance(self,solutions):
        """ Compute the distance of a list of solutions """
        solDistance = {}
        # loop over solutions
        for i in range(len(solutions)):
            solDistance[i] = []
            # each sol got three trucks
            for c in solutions[i]:
                dist = 0
                tourOfTruck = solutions[i][c]
                if len(tourOfTruck):
                    #distance between national bank and first town
                    dist += self.getDistance(self.data.NATIONALBANK,tourOfTruck[0])
                    #distance between towns
                    for j in range(len(tourOfTruck)-1):
                        dist += self.getDistance(tourOfTruck[j],tourOfTruck[j+1])
                    #distance between last town and national bank
                    dist += self.getDistance(tourOfTruck[len(tourOfTruck)-1],self.data.NATIONALBANK)
                    #add distance for each truck
                solDistance[i].append(dist/1000)
        return solDistance



    def computeRisk(self,solutions,distances):
        """ Compute the risk of a list of solution """
        solRisk = {}
        for i in range(len(solutions)):
            solRisk[i] = []
            totRisk = 0
            for c in solutions[i]:
                risk = 0
                amountGathered = 0
                tourOfTruck = solutions[i][c]
                if len(tourOfTruck):
                    #distance between towns
                    for j in range(len(tourOfTruck)-1): 
                        dist = self.getDistance(tourOfTruck[j],tourOfTruck[j+1])/1000
                        amountGathered += self.getSumForTown(tourOfTruck[j])
                        risk += dist * amountGathered
                        if tourOfTruck[j+1] == self.data.NATIONALBANK:
                            totRisk += risk
                            risk = 0
                            amountGathered = 0
                    #distance between last town and national bank
                    dist = self.getDistance(tourOfTruck[len(tourOfTruck)-1],self.data.NATIONALBANK)/1000
                    amountGathered += self.getSumForTown(tourOfTruck[len(tourOfTruck)-1])
                    risk += dist * amountGathered
                    totRisk += risk
                #add risk for each truck
                solRisk[i].append(totRisk)
                totRisk = 0
        return solRisk


    def createIndex(self,dist,risk):
        """ Creates an index of solution """
        ind = {}
        i = 0
        for r,d in zip(risk,dist):
            key = str([risk[r],dist[d]])
            ind[key] = i
            i += 1
        return ind

    def crowdingDistance(self,front):
        """ Sort a front by crowding distance"""
        risks = []
        distances = []
        for sol in front:
            #front contains tuple => (risk,distance)
            risk = sum(sol[0])
            dist = sum(sol[1])
            risks.append(risk)
            distances.append(dist)
        maxDist = max(distances)
        minDist = min(distances)
        maxRisk = max(risks)
        minRisk = min(risks)
        rd = [[risks[i], distances[i]] for i in range(len(risks))]
        temp = sorted(rd, reverse=False)
        sortedFront = []
        distCrowding = {0:[temp[0]]}
        if len(temp) > 1:
            for i in range(1,len(temp)-1):
                di = abs(temp[i-1][0] - temp[i+1][0])/(maxRisk-minRisk)
                di += abs(temp[i-1][1] - temp[i+1][1])/(maxDist-minDist)
                if di in distCrowding:
                    distCrowding[di].append(temp[i])
                else:
                    distCrowding[di] = [temp[i]]
            distCrowding[-1] = [temp[len(temp)-1]]
            od = collections.OrderedDict(sorted(distCrowding.items(),reverse=True))
            for key in od:
                for i in range(len(od[key])):
                    ind = rd.index(od[key][i])
                    sortedFront.append(front[ind])
            return sortedFront
        return front

    def pareto(self,risks,distances):
        """ Return all pareto fronts of a set of solutions """
        listPts = np.array([[sum(risks[r]),sum(distances[d])] for r,d in zip(risks,distances)])
        fronts = []
        #we have to get all the fronts
        while len(listPts):
            # Count number of items
            population_size = listPts.shape[0]
            # Create a NumPy index for listPts on the pareto front (zero indexed)
            population_ids = np.arange(population_size)
            # Create a starting list of items on the Pareto front
            # All items start off as being labelled as on the Parteo front
            pareto_front = np.ones(population_size, dtype=bool)
            # Loop through each item. This will then be compared with all other items
            for i in range(population_size):
                # Loop through all other items
                for j in range(population_size):
                    #print("in for j ",type(listPts))
                    # Check if our 'i' pint is dominated by out 'j' point
                    if all(listPts[j] <= listPts[i]) and any(listPts[j] < listPts[i]):
                        # j dominates i. Label 'i' point as not on Pareto front
                        pareto_front[i] = 0
                        # Stop further comparisons with 'i' (no more comparisons needed)
                        break
            solutions = []
            # Return ids of scenarios on pareto front
            ids = population_ids[pareto_front]
            front = listPts[ids]
            for f in front:
                sol = self.findSol(f,risks,distances)
                solutions.append(sol)
            fronts.append(solutions)
            listPts = np.delete(listPts, ids,axis=0)
        return fronts


    def findSol(self,solution,risks,distances):
        """ From a given a solution return the risks and distances """
        for r,d in zip(risks,distances):
            if np.array_equal(solution, np.array([sum(risks[r]),sum(distances[d])])):
                return [risks[r],distances[d]]


    def nsga2(self,p0,q0):
        """ The core algorithm => create solutions"""
        populationSize = len(p0)
        pj = p0
        qj = q0
        t = 0
        while t < self.tmax:
            print("iter n° : ",t)
            rt = pj + qj
            distance = self.computeDistance(rt)
            risk = self.computeRisk(rt,distance)
            solIndex = self.createIndex(distance,risk)
            paretoFronts = self.pareto(risk,distance)
            pj = []
            j = 0
            #add pareto front to population of sol
            while(len(pj) + len(paretoFronts[j]) <= populationSize):
                for sol in paretoFronts[j]:
                    pj.append(rt[solIndex[str(sol)]])
                j += 1
            #fill the remaining
            if len(pj) < populationSize:
                paretoFronts[j] = self.crowdingDistance(paretoFronts[j])
                k = 0
                while(len(pj) < populationSize):
                    ind = str(paretoFronts[j][k])
                    ind2 = solIndex[ind]
                    pj.append(rt[ind2])
                    k += 1
            #generate children
            qj = self.createChildren(pj)
            t += 1
        distance = self.computeDistance(rt)
        risk = self.computeRisk(rt,distance)
        paretoFronts = self.pareto(risk,distance)
        self.sol = []
        for s in paretoFronts[0]:
            s = rt[solIndex[str(s)]]
            #we need to correct the sols
            self.correctReturns(s)
            self.sol.append(s)
        return paretoFronts





