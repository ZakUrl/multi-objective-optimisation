The repo solves this problem : 

    « Tournées de véhicules sécurisés pour la récupération de l’argent des caisses communales; le cas Bruxellois»

        La région de Bruxelles-Capitale comporte 19 communes. Chaque maison communale
        propose toute une série de services payants à la population. Si les paiements électroniques
        sont de plus en plus courants, il subsiste néanmoins une part importante de la population qui
        préfère payer en “monnaie sonnante et trébuchante”. La caisse communale doit donc être
        relevée à intervalles de temps réguliers et de façon sécurisée. On considère que, par mois,
        le montant payé par habitant est de 70 cents.

        A la fin de chaque mois, des camions blindés partent de la Banque Nationale et passent
        prendre le montant des caisses communales. Ces camions font l’objet de nombreuses
        convoitises, notamment de voleurs et de braqueurs. C’est pourquoi les chemins empruntés
        par ces camions ont une importance particulière. Ils doivent non seulement emprunter le
        chemin qui soit le plus court possible mais également emprunter des chemins qui soient les
        plus sécurisés possible. Pour ce faire, trois camions sont disponibles et se partagent les
        tournées à réaliser. 
        Ces tournées doivent simultanément:
        • minimiser la distance totale parcourue;
        • minimiser le “risque” qui est modélisé comme étant la distance totale parcourue
        pondérée par le montant total présent dans le camion en cours de trajet.
        Un certain nombre de contraintes doivent également être satisfaites:
        • à aucun moment donné un camion ne peut transporter plus de 50% du montant total
        à récupérer;
        • les trois communes les plus peuplées ne peuvent pas être visitées par un même
        camion;
        On vous demande d’établir la frontière Pareto optimale de ce problème.

To launch : python3 main.py