import csv

class CSVCreator:
    """ Creates a csv file from the data received in parameters """
    def __init__(self,filename,data):
        self.filename = filename
        self.data = data

    def create(self):
        """ Write the data to a csv file"""
        with open(self.filename, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=';')
            for sol, amount in zip(self.data[0],self.data[1]):
                for c in sol:
                    spamwriter.writerow(["0"] + sol[c])
                    spamwriter.writerow(amount[c])




